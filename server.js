const express = require('express');
const http = require('http');
var fs = require('fs'); 

const app = express();
const port = process.env.PORT || 5000;

app.use(express.static('images'))

var metadataS = JSON.parse(fs.readFileSync('data/templates.json', 'utf8'));

app.get('/api/data', (req, res) => { //changing api/xxxx changes path to send data
  res.send({metadata:metadataS});
});

app.listen(port, () => console.log(`Listening on port ${port}`));