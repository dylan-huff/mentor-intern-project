import React, { Component } from 'react';

import './style.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      current: 0,
      currentImage: 0,
      metadata:[],
      title:'',
      cost:'',
      id:'',
      desc:'',
      thumbnailName:'',
      imageName:''
    };

    this.handleClickNext = this.handleClickNext.bind(this);
    this.handleClickPrev = this.handleClickPrev.bind(this);
    this.nextArrow = this.nextArrow.bind(this);
    this.prevArrow = this.prevArrow.bind(this);
    this.changeImage = this.changeImage.bind(this);
    this.ContainerView = this.ContainerView.bind(this);
    this.mainImage = this.mainImage.bind(this);
    this.getThumbnail = this.getThumbnail.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ metadata: res.metadata.slice(0), title: res.metadata[0].title, cost: res.metadata[0].cost,
       id: res.metadata[0].id, desc: res.metadata[0].description, thumbnailName: res.metadata[0].thumbnail,
       imageName: res.metadata[0].image }))
      .catch(err => console.log(err));
  }

  handleClickNext(){
    if ((this.state.current + 4)>15){
      this.setState({current: 15})
    } else {
      this.setState(state => ({current: (state.current + 4)}));
    }
  }

  handleClickPrev(){
    if ((this.state.current - 4)<=0){
      this.setState({current: 0})
    } else { 
      this.setState(state => ({current: (state.current - 4 )}));
    }
  }

  nextArrow(){
    if (this.state.current < 12){
      return (
        <a className="next" title="next">
          <img onClick={this.handleClickNext} src='http://localhost:5000/next.png' alt="next" />
        </a>
      );
    } else {
      return (
        <a className="next disabled" title="next">
          <img onClick={this.handleClickNext} src='http://localhost:5000/next.png' alt="next" />
        </a>
      );
    }
  }

  prevArrow(){
    if (this.state.current >= 4){
      return (
        <a className="previous" title="previous">
          <img id="previous" onClick={this.handleClickPrev} src='http://localhost:5000/previous.png' alt="previous" />
        </a>
      );
    } else {
      return (
        <a className="previous disabled" title="previous">
          <img id="previous" onClick={this.handleClickPrev} src='http://localhost:5000/previous.png' alt="previous" />
        </a>
      );
    }
  }

  changeImage(num){
    this.setState({title: this.state.metadata[num].title, cost: this.state.metadata[num].cost,
      id: this.state.metadata[num].id, desc: this.state.metadata[num].description,
      thumbnailName: this.state.metadata[num].thumbnail,
      imageName: this.state.metadata[num].image, currentImage: num});
  }

  mainImage(id){
    var newSrc = "http://localhost:5000/large/"+this.state.imageName;
    return (<img src={newSrc} alt={this.state.imageName} width="430" height="360" />);
  }

  getThumbnail (props){
    if (this.state.metadata.length > 0){
      var newSrc = "http://localhost:5000/thumbnails/"+this.state.metadata[props.thumbnum].thumbnail;
      if (props.thumbnum === this.state.currentImage){
            return(
              <a className = "active" title={this.state.metadata[props.thumbnum].thumbnail}>
                <img src={newSrc} alt={this.state.metadata[props.thumbnum].id} width ="145" height="121" onClick={this.changeImage.bind(null,props.thumbnum)}/>
                <span>{this.state.metadata[props.thumbnum].id}</span>
              </a>
            );
      } else {
        return(
          <a title={this.state.metadata[props.thumbnum].thumbnail}>
            <img src={newSrc} alt={this.state.metadata[props.thumbnum].id} width ="145" height="121" onClick={this.changeImage.bind(null,props.thumbnum)}/>
            <span>{this.state.metadata[props.thumbnum].id}</span>
          </a>
        );
      }
    } else {
      return (null);
    }
  }

  ContainerView(){
    if (this.state.current < 4) {
      return (
        <div className="thumbnails">
          <div className="group">
            <this.prevArrow/>
            <this.getThumbnail thumbnum={0}/>
            <this.getThumbnail thumbnum={1}/>
            <this.getThumbnail thumbnum={2}/>
            <this.getThumbnail thumbnum={3}/>
            <this.nextArrow/>
          </div>
        </div>
      )
    } else if ((this.state.current>=4)&&(this.state.current<8)) {
      return (
        <div className="thumbnails">
          <div className="group">
            <this.prevArrow/>
            <this.getThumbnail thumbnum={4}/>
            <this.getThumbnail thumbnum={5}/>
            <this.getThumbnail thumbnum={6}/>
            <this.getThumbnail thumbnum={7}/>
            <this.nextArrow/>
          </div>
        </div>
      )
    } else if ((this.state.current>=8)&&(this.state.current<12)) {
      return (
        <div className="thumbnails">
          <div className="group">
            <this.prevArrow/>
            <this.getThumbnail thumbnum={8}/>
            <this.getThumbnail thumbnum={9}/>
            <this.getThumbnail thumbnum={10}/>
            <this.getThumbnail thumbnum={11}/>
            <this.nextArrow/>
          </div>
        </div>
      )
    } else {
      return (
        <div className="thumbnails">
          <div className="group">
            <this.prevArrow/>
            <this.getThumbnail thumbnum={12}/>
            <this.getThumbnail thumbnum={13}/>
            <this.getThumbnail thumbnum={14}/>
            <this.nextArrow/>
          </div>
        </div>      )
    }
  }

  callApi = async () => {
    const response = await fetch('/api/data');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  render() {
    return (
      <body>
        <div id="container">
          <header> Coding Interview Project </header>
          <div id="main" role="main">
            <div id="large">
              <div className="group">
                <this.mainImage />
                <div className= "details">
                  <p><strong>Title</strong> {this.state.title} </p>
                  <p><strong>Description</strong> {this.state.desc}</p>
                  <p><strong>Cost</strong> ${this.state.cost}</p>
                  <p><strong>ID #</strong> {this.state.id}</p>
                  <p><strong>Image Name</strong> {this.state.thumbnailName}</p>
                  <p><strong>Thumbnail Name</strong> {this.state.imageName}</p>
                </div>
              </div>
            </div>
            <this.ContainerView/>
          </div>
        </div>
      </body>
    );
  }
}


export default App;

/// highlight thumbnail of currently viewing
//generally clean up