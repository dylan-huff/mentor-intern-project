To run this app you will need the following packages:
	Node
	Express
	React
	Yarn
	NPM

To run this project, run "npm install react react-dom" in "dylan-huff-mentor-intern-project".Once completed, run "yarn install" in the same directory. Finally, run "yarn dev" in the same directory.

This should open a browser with the website. If the page doesn't autoload, go to "http://localhost:3000/".

The backend API is in dylan-huff-mentor-intern-project/server.js

The client is in dylan-huff-mentor-intern-project/client/src/App.js

Questions? Email huffd@reed.edu

-Dylan